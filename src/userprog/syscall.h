#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <stdbool.h>
#include "threads/thread.h"

typedef int pid_t;
#define PID_ERROR ((pid_t) -1)

void syscall_init (void);
void halt(void);
bool create(const char* file, unsigned initial_size);
bool remove(const char* file);
void exit(int status);
int wait(tid_t tid);
pid_t exec(const char* cmd_line);
void check_address(void *addr);
void get_argument(void *esp, int *arg, int count);

/* Assignment 4 for file lock */
struct lock filesys_lock;
/* Assggnment 4 for syscall */

int open(const char *file);
int filesize(int fd);
int read (int fd,void *buffer, unsigned size);
int write(int fd,void *buffer, unsigned size);
void seek(int fd, unsigned position);
unsigned tell(int fd);
void close(int fd);

#endif /* userprog/syscall.h */
