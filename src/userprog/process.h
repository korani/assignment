#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
/* Assginment 4 for file */
#include <list.h>
#include <filesys/file.h>


struct thread* get_child_process(int pid);
void remove_child_process(struct thread* cp);

void argument_stack(char **parse, int count, void **esp);
tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);

/* Assignment 4 */

int process_add_file(struct file *f);
struct file *process_get_file(int fd);
void process_close_file(int fd);

struct file_elem{
	int fd;
	struct file *f;
	struct list_elem elem;
}; // struct for file_table

#endif /* userprog/process.h */
