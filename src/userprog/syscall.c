#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include <devices/shutdown.h>
#include <filesys/filesys.h>
#include "userprog/process.h"
#include <devices/input.h>

static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{ 
  /* Assignment4 for file lock*/
  lock_init(&filesys_lock);

  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f) 
{
  void* esp;
  int sys_num; // syscall_number
  int args[3]; // system call arguments (there can be up to 3 arguments)

  /* get stack pointer from interrupt frame */
  esp = f->esp;
  check_address(esp); // check if the address is valid

  /* get system call number from stack */
  sys_num = *(int *) esp;

  //printf ("system call!\n"); 

  switch (sys_num)
  {
  /*Assignment4 for file syscall */
  case SYS_OPEN:
      //open() takes 1 argument(const char*)
    get_argument(esp,args,1);

    check_address(args[0]);

    f->eax = open((const char *)args[0]);
    break;

  case SYS_FILESIZE:
      //filesize takes 1 argument(int fd)
    get_argument(esp,args,1);
    f->eax = filesize(args[0]);
    break;

  case SYS_READ:
      //read take 3 argument(int,void*,unsigned)
    get_argument(esp,args,3);

    check_address(args[1]);

    f->eax = read(args[0],(void*)args[1],(unsigned)args[2]);
    break;
  case SYS_WRITE:
      //write takes 3 argument(int,void*,unsigend)
    get_argument(esp,args,3);

    check_address(args[1]);

    f->eax = write(args[0],(void*)args[1],(unsigned)args[2]);
    break;
  case SYS_SEEK:
      //seek takes 2 argument(int,unsiged)
    get_argument(esp,args,2);
    seek(args[0],(unsigned)args[1]);
    break;
  case SYS_TELL:
      //tell takes 1 argument(int)
    get_argument(esp,args,1);
    f->eax = tell(args[0]);
    break;
  case SYS_CLOSE:
      //close takes 1 argument(int)
    get_argument(esp,args,1);
    close(args[0]);
    break;

  case SYS_WAIT:
    // wait() takes 1 argument (tid_t)
    get_argument(esp, args, 1);

    // set the return value at %eax
    f->eax = wait((tid_t) args[0]);
    break;
  case SYS_EXEC:
    // exec() takes 1 argument (const char*)
    get_argument(esp, args, 1);

    // address checking needed for pointer type arguments
    check_address((void *) args[0]);
    
    // set the return value at %eax
    f->eax = exec((char *) args[0]);
    break;
	case SYS_HALT:
    // halt() takes no argument.
	  halt();
	  break;
	case SYS_EXIT:
    // exit() takes 1 argument (int) 
	  get_argument(esp, args, 1);

	  exit(args[0]);
	  break;
	case SYS_CREATE:
    // create() takes 2 arguments (const char*, unsigned)
	  get_argument(esp, args, 2);

    // address checking needed for pointer type arguments
    check_address((void *) args[0]);

    // set the return value at %eax
	  f->eax = create((char *) args[0], (unsigned) args[1]);
	  break;
	case SYS_REMOVE:
    // remove() takes 1 argument (const char*)
	  get_argument(esp, args, 1);

    // address checking needed for pointer type arguments
    check_address((void *) args[0]);

    // set the return value at %eax
	  f->eax = remove((char *) args[0]);
	  break;
  }

   //thread_exit ();
}


void check_address(void *addr)
{
  /* check if the address is within the boundary of user area */
  /* user memory starts at 0x08048000, and the kernel memory starts at 0xc0000000 */
  if ((unsigned int) addr >= 0xc0000000 || (unsigned int) addr < 0x08048000){
    // terminate the process...
	  exit(-1);
	}
  
}


void get_argument(void *esp, int *arg, int count)
{
	int i;
	int *ptr;
	
	ptr = esp + 4;
	for (i = 0; i < count; i++)
  {
	  check_address(ptr + i);
  	arg[i] = *(ptr + i);
	}

}

void halt(void)
{
  /* quit pintos using shutdown_power_off() */
  shutdown_power_off();
}

void exit(int status)
{
  struct thread* current_thread = NULL;
  /* get currently running thread struct */
  current_thread = thread_current();

  /* store exit_status in the thread struct */
  current_thread->exit_status = status; 

  /* print out process termination message */
  printf("%s: exit(%d)\n", current_thread->name, status);

  /* exit thread */
  thread_exit();
}

bool create(const char* file, unsigned initial_size)
{
  /* create file using filesys_create() */
  bool success = filesys_create(file, initial_size);
  return success;
}

bool remove(const char* file)
{
  /* remove file using filesys_remove() */
  bool success = filesys_remove(file);
  return success;
}

pid_t exec(const char* cmd_line)
{
  /* create and execute a new child process */
  int child_pid;
  struct thread* child_thread;

  child_pid = process_execute(cmd_line); // process_execute() returns PID_ERROR(-1) when failed to execute the child process
  if (child_pid == PID_ERROR)
    return PID_ERROR;

  child_thread = get_child_process(child_pid);
  sema_down(&(child_thread->sema_load)); // wait until the child process is loaded onto memory

  if (child_thread->loaded == false)
    return PID_ERROR;

  return child_pid;
}

int wait(tid_t tid)
{
  return process_wait(tid);
}

/* Assignment4 plus function */
int open(const char *file){
  int fd;
  struct file *f = NULL;
 
  f = filesys_open(file);

  if (f == NULL)
    return -1;

  fd = process_add_file(f);
  return fd;
}

int filesize(int fd) {
  int length;
  struct file *f = NULL;

  f = process_get_file(fd);

  if (f == NULL)
    return -1;

  length = file_length(f);
  return length;
}

int read(int fd, void *buffer, unsigned size){
  
  int i; 
  struct file *f = NULL;
  uint8_t *addr = NULL;

  int result;

  /* acquire lock before starting the routine. */
  lock_acquire(&filesys_lock);
 
  /* Case 1: Reading from STDIN */
  if(fd == 0) {   // STDIN
	  addr = (uint8_t *) buffer;
    /* Read from STDIN by [size] bytes */
    for(i = 0; i < size; i++) {
	    *addr = input_getc();
      addr = addr + 1;
    }
	  result = size;
  }
  /* Case 2: Reading from a file other than STDIN */
  else {
    f = process_get_file(fd);
    if (f == NULL) {
      /* in case of exceptional situations, return -1. */
	    result = -1;
    }
    else {
      /* read from the file and return the number of bytes read. */
      result = file_read(f, buffer, size);
    }
  }

  /* release lock before returning */
  lock_release(&filesys_lock);
  return result;
}

int write(int fd, void *buffer, unsigned size){
  struct file *f = NULL;
  int result;

  lock_acquire(&filesys_lock);

  /* Case 1: Writing to STDOUT */
  if (fd == 1) { //STDOUT
    /* write to STDOUT via putbuf() */
	  putbuf(buffer, size);
	  result = size;
  }
  /* Case 2: Writing to a file other than STDOUT */
  else {
    f = process_get_file(fd);

    if (f == NULL) {
      result = -1;
    } else {
      result = file_write(f, buffer, size);
    }
  }

  lock_release(&filesys_lock);
  return result;
}

void seek (int fd, unsigned position) {
  struct file *f = NULL;
  f = process_get_file(fd);
  file_seek(f, position);
}

unsigned tell (int fd) {
  struct file *f = NULL;
  f = process_get_file(fd);
  return (unsigned) file_tell(f);
}

void close (int fd) {
  process_close_file(fd);
}

