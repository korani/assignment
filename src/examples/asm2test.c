#include <stdio.h>
#include <syscall.h>
#include <string.h>

int main(int argc, char** argv)
{
  int i;
  bool result;
  if (argc == 3)
  {
    if (strcmp(argv[1], "-r") == 0)
    {
      result = remove(argv[2]);
      printf("remove %ssuccessful.\n", result ? "" : "un");
    }
    else if (strcmp(argv[1], "-c") == 0)
    {
      result = create(argv[2], 1);
      printf("create %ssuccessful.\n", result ? "" : "un");
    }
  }
  else if (argc == 2)
  {
    if (strcmp(argv[1], "-h") == 0)
    {
      halt();
    }
  }
  else
  {
    exit(argc);
  }
}
